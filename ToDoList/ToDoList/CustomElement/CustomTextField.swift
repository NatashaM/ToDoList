//
//  CustomTextField.swift
//  ToDoList
//
//  Created by Наталья Миронова on 22.06.2023.
//

import UIKit

//MARK: - RegisterTextField
final class CustomTextField: UITextField {
	
	private let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
	
	//MARK: - Initializers
	init(placeholder: String) {
		super.init(frame: .zero)
		setupTextField(placeholder: placeholder)
	}
	
	@available(*, unavailable)
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	//MARK: - Override Methods
	override func textRect(forBounds bounds: CGRect) -> CGRect {
		bounds.inset(by: padding)
	}
	
	override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
		bounds.inset(by: padding)
	}
	
	override func editingRect(forBounds bounds: CGRect) -> CGRect {
		bounds.inset(by: padding)
	}
	
	private func setupTextField(placeholder: String) {
		layer.cornerRadius = 10
		tintColor = .black
		layer.backgroundColor = #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
		
		attributedPlaceholder = NSAttributedString(string: placeholder, attributes:
													[NSAttributedString.Key.foregroundColor : UIColor.lightGray])
		
		font = .systemFont(ofSize: 20)
		heightAnchor.constraint(equalToConstant: 60).isActive = true
	}
}
