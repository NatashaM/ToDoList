//
//  StorageManager.swift
//  ToDoList
//
//  Created by Наталья Миронова on 21.06.2023.
//

import CoreData

final class StorageManager {
	
	static let shared = StorageManager()
	
	// MARK: - Core Data stack
	private let persistentContainer: NSPersistentContainer = {
		let container = NSPersistentContainer(name: "ToDoList")
		container.loadPersistentStores { _, error in
			if let error = error as NSError? {
				fatalError("Unresolved error \(error), \(error.userInfo)")
			}
		}
		return container
	}()
	
	private let viewContext: NSManagedObjectContext
	
	private init() {
		viewContext = persistentContainer.viewContext
	}
	
	// MARK: - CRUD
	/// Метод создает задачу.
	/// - Parameters:
	///   - taskName: Название задачи.
	///   - taskInfo: Подробная информация о задаче.
	///   - taskDate: Дата выполнения.
	func create(_ taskName: String, _ taskInfo: String, _ taskDate: Date?) {
		let task = Task(context: viewContext)
		task.title = taskName
		task.info = taskInfo
		task.date = taskDate
		saveContext()
	}
	
	/// Метод загружает список задач из базы.
	/// - Parameter completion: Возвращает тип Result, который содержит в себе либо список задач, либо ошибку.
	func fetchData(completion: (Result<[Task], Error>) -> Void) {
		let fetchRequest = Task.fetchRequest()
		
		do {
			let tasks = try viewContext.fetch(fetchRequest)
			completion(.success(tasks))
		} catch let error {
			completion(.failure(error))
		}
	}
	
	/// Метод обновляет полученную задачу.
	/// - Parameters:
	///   - task: Задача, которую необходимо обновить.
	///   - newName: Новый title задачи.
	///   - newInfo: Новое описание задачи.
	///   - newDate: Новая дата задачи.
	func update(_ task: Task, newName: String, newInfo: String, newDate: Date?) {
		task.title = newName
		task.info = newInfo
		task.date = newDate
		saveContext()
	}
	
	/// Удаление задачи из базы.
	/// - Parameter task: Задача, которую необходимо удалить.
	func delete(_ task: Task) {
		viewContext.delete(task)
		saveContext()
	}
	
	// MARK: - Core Data Saving support
	/// Сохранение изменений в контекст.
	func saveContext() {
		if viewContext.hasChanges {
			do {
				try viewContext.save()
			} catch {
				let nserror = error as NSError
				fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
			}
		}
	}
}
