//
//  ToDoListViewController.swift
//  ToDoList
//
//  Created by Наталья Миронова on 20.06.2023.
//

import UIKit

protocol TaskViewControllerDelegate {
	/// Производит загрузку данных
	func reloadData()
}

//MARK: - ToDoListViewController
final class ToDoListViewController: UITableViewController {
	
	private let cellID = "task"
	private let storageManager = StorageManager.shared
	private var taskList: [Task] = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupView()
		fetchData()
	}
	
	// MARK: - Actions
	@objc
	private func addNewTask() {
		let taskVC = TaskViewController()
		taskVC.delegate = self
		navigationController?.pushViewController(taskVC, animated: true)
	}
	
	private func fetchData() {
		storageManager.fetchData { [unowned self] result in
			switch result {
			case .success(let taskList):
				self.taskList = taskList.sorted {
					guard let date = $0.date else {
						return false
					}
					guard let dateNext = $1.date else {
						return true
					}
					return date < dateNext
				}
			case .failure(let error):
				print(error.localizedDescription)
			}
		}
	}
}

//MARK: - Setting View
private extension ToDoListViewController {
	func setupView() {
		view.backgroundColor = .white
		
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
		
		setupNavigationBar()
	}
}

//MARK: - Setting
private extension ToDoListViewController {
	
	func  setupNavigationBar() {
		title = "To do list"
		
		navigationController?.navigationBar.prefersLargeTitles = true
		
		let navBarAppearance = UINavigationBarAppearance()
		navBarAppearance.backgroundColor = #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
		navigationController?.navigationBar.standardAppearance = navBarAppearance
		navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
		navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNewTask))
		navigationController?.navigationBar.tintColor = .black
	}
}

// MARK: - UITableViewDataSource
extension ToDoListViewController {
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		taskList.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellID)
		let task = taskList[indexPath.row]
		if #available(iOS 14.0, *) {
			var content = cell.defaultContentConfiguration()
			content.text = task.title
			content.secondaryText = task.info
			cell.contentConfiguration = content
		} else {
			cell.textLabel?.text = task.title
			cell.detailTextLabel?.text = task.info
		}
		
		let dueDate = UILabel(frame: CGRect(x: 0, y: 0, width: 80, height: 20))
		dueDate.font = .systemFont(ofSize: 14)
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd.MM.yyyy"
		if let date = task.date {
			dueDate.text = dateFormatter.string(from: date)
		}
		dueDate.textAlignment = .right
		cell.accessoryView = dueDate
		return cell
	}
}

extension ToDoListViewController: TaskViewControllerDelegate {
	func reloadData() {
		fetchData()
		tableView.reloadData()
	}
}

// MARK: - UITableViewDelegate
extension ToDoListViewController {
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		let task = taskList[indexPath.row]
		let taskVC = TaskViewController()
		taskVC.delegate = self
		taskVC.sendTask(task)
		
		navigationController?.pushViewController(taskVC, animated: true)
	}
	
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			let task = taskList.remove(at: indexPath.row)
			tableView.deleteRows(at: [indexPath], with: .automatic)
			storageManager.delete(task)
		}
	}
}
