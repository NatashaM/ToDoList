//
//  TaskViewController.swift
//  ToDoList
//
//  Created by Наталья Миронова on 21.06.2023.
//

import UIKit

final class TaskViewController: UIViewController {
	
	var delegate: TaskViewControllerDelegate!
	
	private let storageManager = StorageManager.shared
	
	private let taskTextField = CustomTextField(placeholder: "Title")
	
	private let taskNoteTextView = CustomTextField(placeholder: "Note")
	private let dateTaskTextField = CustomTextField(placeholder: "Due date")
	
	private var datePicker = UIDatePicker()
	private let toolBar = UIToolbar()
	private var saveBarButton = UIBarButtonItem()
	
	private let dateFormatter = DateFormatter()
	private var task: Task?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupView()
	}
	
	@objc
	func donePressed() {
		dateFormatter.dateFormat = "dd.MM.yyyy"
		dateTaskTextField.text = dateFormatter.string(from: datePicker.date)
		
		dateTaskTextField.resignFirstResponder()
	}
	
	@objc
	func saveTask() {
		if let task = task {
			update(task: task)
		} else {
			save()
		}
		delegate.reloadData()
		navigationController?.popViewController(animated: true)
	}
	
	func sendTask(_ task: Task) {
		self.task = task
	}
	
	private func save() {
		storageManager.create(
			taskTextField.text ?? "",
			taskNoteTextView.text ?? "",
			dateFormatter.date(from: dateTaskTextField.text ?? "") ?? nil
		)
	}
	
	private func update(task: Task) {
		storageManager.update(
			task,
			newName: taskTextField.text ?? "",
			newInfo: taskNoteTextView.text ?? "",
			newDate: dateFormatter.date(from: dateTaskTextField.text ?? "") ?? nil
		)
	}
}

//MARK: - Setting View
private extension TaskViewController {
	func setupView() {
		view.backgroundColor = .white
		
		addSubViews()
		
		setupNavigationBar()
		setupTaskTextField()
		setupTaskNoteTextView()
		setupDatePicher()
		setupTextFieldData()
		settingsToolBarItems()
		
		setupLayout()
	}
}

//MARK: - Setting
private extension TaskViewController {
	
	func addSubViews() {
		[taskTextField,
		 taskNoteTextView,
		 dateTaskTextField].forEach {
			view.addSubview($0)
		}
	}
	
	func setupNavigationBar() {
		saveBarButton = UIBarButtonItem(
			title: "Save",
			style: .plain,
			target: self,
			action: #selector(saveTask)
		)
		saveBarButton.isEnabled = false
		
		navigationItem.rightBarButtonItem = saveBarButton
		
		navigationController?.navigationBar.tintColor = .black
	}
	
	func setupTaskTextField() {
		taskTextField.delegate = self
		taskTextField.becomeFirstResponder()
		taskTextField.text = task?.title
	}
	
	func setupTaskNoteTextView() {
		taskNoteTextView.text = task?.info
		
	}
	
	func setupDatePicher() {
		datePicker.datePickerMode = .date
		datePicker.timeZone = TimeZone.current
		datePicker.minimumDate = Date()
		datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 100, to: Date())
		datePicker.locale = Locale(identifier: "ru_RU")
		
		if #available(iOS 14, *) {
			datePicker.preferredDatePickerStyle = .wheels
		} else {
			datePicker.datePickerMode = .dateAndTime
		}
	}
	
	func setupTextFieldData() {
		dateFormatter.dateFormat = "dd.MM.yyyy"
		if let date = task?.date {
			dateTaskTextField.text = dateFormatter.string(from: date)
		}
		dateTaskTextField.inputView = datePicker
		dateTaskTextField.inputAccessoryView = toolBar
	}
	
	func settingsToolBarItems() {
		
		let doneButtonItem = UIBarButtonItem(
			title: "Done",
			style: .done,
			target: self,
			action: #selector(donePressed)
		)
		doneButtonItem.tintColor = .black
		
		let flexibleSpace = UIBarButtonItem(
			barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
			target: nil,
			action: nil
		)
		toolBar.sizeToFit()
		toolBar.backgroundColor = .gray
		toolBar.setItems([flexibleSpace, doneButtonItem], animated: true)
	}
}

//MARK: - Layout
private extension TaskViewController {
	
	func setupLayout() {
		
		[taskTextField, taskNoteTextView, dateTaskTextField, datePicker].forEach {
			$0.translatesAutoresizingMaskIntoConstraints = false
		}
		
		NSLayoutConstraint.activate([
			taskTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
			taskTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			taskTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.80),
			
			taskNoteTextView.topAnchor.constraint(equalTo: taskTextField.bottomAnchor, constant: 20),
			taskNoteTextView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			taskNoteTextView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.80),
			
			dateTaskTextField.topAnchor.constraint(equalTo: taskNoteTextView.bottomAnchor, constant: 20),
			dateTaskTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			dateTaskTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.80)
		])
	}
}

//MARK: - UITextFieldDelegate
extension TaskViewController: UITextFieldDelegate {
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		taskNoteTextView.becomeFirstResponder()
		return true
	}
	
	func textFieldDidChangeSelection(_ textField: UITextField) {
		if let text = textField.text {
			saveBarButton.isEnabled = !text.isEmpty
		}
	}
}
