//
//  AppDelegate.swift
//  ToDoList
//
//  Created by Наталья Миронова on 20.06.2023.
//

import UIKit

@main
final class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
	private var storageManager = StorageManager.shared
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		window = UIWindow(frame: UIScreen.main.bounds)
		window?.makeKeyAndVisible()
		window?.rootViewController = UINavigationController(rootViewController: ToDoListViewController())
		return true
	}
	
	func applicationWillTerminate(_ application: UIApplication) {
		storageManager.saveContext()
	}
}

